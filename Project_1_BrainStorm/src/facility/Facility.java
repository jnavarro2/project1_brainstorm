package facility;

public class Facility 
{
	///BRAINSTORM!!!! NOT ACTUAL CODE OR STRUCTURE
	public void listFacilities()
	{
		//Create a string list of two strings. One being Name, second being information
		//Have to have an integer to show capacity of each facility
		//Integer is associated with the individual facility.

		/*DARYA - how about a multidimensional array? I don't think java supports structs (C++), but
		I'm pretty sure java does have X-D arrays, with X being the number of components we want.
		For example, indeces would corresp which facility, but first row would be facility name, second row would be max capacity, etc.

		Though, I'm not sure if this is how he wants to organize facility information, since the rest of project is just a bunch of
		getters and setters :(
		 */
		 
		 /* Carl
		 	What about using a hashmap?   IE
			HashMap<Int, Facility> where the INT is the number of the facility and the Facility is the actual object? We could loop through the map and then get
			all the details from the actual facility object
		 */
	}
	public void getFacilityInformation()
	{
		//Call listFacilities() and have the user show which facility they want to know more

		//DARYA - see above comment about how we're going to organize facility information, that's going to effect how we access/define
		//these getters and setters
	}
	public void requestAvaliableCapacity()
	{
		//Call listFacilities() and get the capacity of the facility
		
		/*  Carl
		   Capacity should return an INT ?
		   public Int requestAvailableCapacity(Facility facName){
		   		return facName.getCapacity
		   }
		*/
	}
	public void addNewFacility()
	{
		//MUST BE VOID
		//Create new facility, add to the string list of two strings
	}
	public void addFacilityDetail()
	{
		//Create new facility_detail, add to the string list of two strings
	}
	public void removeFacility()
	{
		//remove a facility in the string list (or vector)
	}
}
