package inspection;
import LocalDate;
import LocalTime;

public interface Inspection {

    public void setInspectorName(String name);
    public String getInspection_inspector_name();

    public void setInspectionDate(LocalDate inspection_date);
    public LocalDate getInspection_date();

    public void setInspectionRequestDate(LocalDate inspection_date);
    public LocalDate getInspection_request_date();

    public void setInspectionTime(LocalTime inspectionTime);
    public LocalTime getInspection_time();

    public void setInspectionRequestTime(LocalTime inspectionTime);
    public LocalTime getInspection_request_time();

    public void setInspectionIssue(String issue);
    public String getInspection_issue();

    public void setInspectionPassed(Boolean passed);
    public Boolean getInspection_passed();

}
