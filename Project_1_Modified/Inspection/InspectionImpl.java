package inspection;
import java.time.LocalDate;
import java.time.LocalTime;

public class Inspection implements Inspection {
    public String inspector_name;
    public LocalDate inspection_date;
    public LocalDate inspection_request_date;
    public LocalTime inspection_time;
    public LocalTime inspection_request_time;
    public String issue;
    public Boolean inspection_passed;

    public void setInspectorName(String name){this.inspector_name = name;}

    public String getInspection_inspector_name(){ return this.inspector_name; }

    public void setInspectionDate(LocalDate inspection_date){this.inspection_date = inspection_date;}

    public LocalDate getInspection_date(){ return this.inspection_date; }

    public void setInspectionRequestDate(LocalDate inspection_date){this.inspection_request_date = inspection_date;}

    public LocalDate getInspection_request_date(){ return this.inspection_request_date; }

    public void setInspectionTime(LocalTime inspectionTime){this.inspection_time = inspectionTime;}

    public LocalTime getInspection_time(){ return this.inspection_time; }

    public void setInspectionRequestTime(LocalTime inspectionTime){this.inspection_request_time = inspectionTime;}

    public LocalTime getInspection_request_time(){ return this.inspection_request_time; }

    public void setInspectionIssue(String issue){this.issue = issue;}

    public String getInspection_issue(){ return this.issue; }

    public void setInspectionPassed(Boolean passed){this.inspection_passed = passed;}

    public Boolean getInspection_passed() { return this.inspection_passed; }


}
