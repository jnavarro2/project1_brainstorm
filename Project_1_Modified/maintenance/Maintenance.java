package maintenance;
import java.time.LocalDateTime;
import java.util.ArrayList;

import maintenance.MaintenanceImpl;
import maintenance.MaintenanceImpl.status;

import java.time.format.DateTimeFormatter;
public interface Maintenance 
{
	public Maintenance getMaintenanceRequest(String name);
	public int getSchedule_time();
	public void setSchedule_time(int time);
	public int getTotal_cost();
	public void setTotal_cost(int cost);
	public int getTotal_budget();
	public void setTotal_budget(int budget);
	public void setRequest_approval(boolean req);
	public boolean getRequest_approval();
	public void setProblem_approval(boolean prob);
	public boolean getProblem_approval();
	public void setProblem(String problem);
	public String getProblem();
	public void setMoney_approval(boolean mon);
	public boolean getMoney_approval();
	public void setStatus(status input);
	public status getStatus();
	public void makeFacilityRequest(ArrayList<Maintenance> main);
	public void addMaintenance(ArrayList<Maintenance> main);
	public void setMainenanceName(String s);
	public String getMaintenance_name();
	public void calcMaintenanceCostForFacility(Maintenance main);
	public int calcProblemRateForFacility(int current_capacity, int max_capacity, Maintenance main);
	public void calcDownTimeForFacility();
	public void listMaintRequests();
	public void listMaintenance();
	public void listFacilityProblems();
	public void setMaintenanceRequestBeginTime();
	public LocalDateTime getMaintenanceRequestBeginTime();
	public void setMaintenanceRequestCompletionTime();
	public LocalDateTime getMaintenanceRequestCompletionTime();
	
}
